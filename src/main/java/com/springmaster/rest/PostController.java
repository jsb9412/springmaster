package com.springmaster.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.springmaster.service.PostServiceImpl;

@Controller
public class PostController implements BasicController {

    @Autowired
    private PostServiceImpl postService;

    public String addPost(String content) {
        return postService.addPost(content);
    }

}
