package com.springmaster.context;

import java.util.logging.Logger;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.springmaster.rest.PostController;

public class LaunchContext {

    private static final String CONTENT = "Test!!!";

    public static Logger logger = Logger.getLogger(LaunchContext.class.getName());

    public static void main(String[] args) {
        ApplicationContext context = new
                AnnotationConfigApplicationContext(AppContext.class);

        PostController controller = context.getBean(PostController.class);
        logger.info(controller.addPost(CONTENT));
    }
}
