package com.springmaster.service;

import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

    @Override
    public String addPost(String content) {
        return "Your post is " + content;
    }
}
